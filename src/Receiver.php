<?php

declare(strict_types=1);

namespace SlyFoxCreative\ExceptionReporter;

use Illuminate\Notifications\Notifiable;

class Receiver
{
    use Notifiable;

    public function routeNotificationForSlack(): string
    {
        return config('exception_reporter.slack_webhook_url');
    }
}
