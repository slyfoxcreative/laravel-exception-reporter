<?php

declare(strict_types=1);

namespace SlyFoxCreative\ExceptionReporter;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;

class ExceptionThrown extends Notification implements ShouldQueue
{
    use Queueable;

    private $class;
    private $file;
    private $line;
    private $message;

    public function __construct(string $class, string $file, int $line, string $message)
    {
        $this->class = $class;
        $this->file = $file;
        $this->line = $line;
        $this->message = $message;
    }

    public function via($notifiable): array
    {
        return ['slack'];
    }

    public function toSlack($notifiable): SlackMessage
    {
        $appName = config('app.name');

        return (new SlackMessage)
            ->error()
            ->content("Exception in $appName, $this->file:$this->line")
            ->attachment(function ($attachment) {
                $attachment
                    ->title($this->class)
                    ->content($this->message);
            });
    }
}
