<?php

declare(strict_types=1);

namespace SlyFoxCreative\ExceptionReporter;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    private const CONFIG_FILE = __DIR__ . '/config/exception_reporter.php';

    public function boot()
    {
        $this->publishes([
            self::CONFIG_FILE => config_path(basename(self::CONFIG_FILE)),
        ]);
    }

    public function register()
    {
        $this->mergeConfigFrom(
            self::CONFIG_FILE,
            basename(self::CONFIG_FILE, '.php')
        );
    }
}
