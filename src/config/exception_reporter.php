<?php

return [
    'slack_webhook_url' => env('EXCEPTION_REPORTER_SLACK_WEBHOOK_URL'),
];
