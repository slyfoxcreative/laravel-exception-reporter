<?php

declare(strict_types=1);

namespace SlyFoxCreative\ExceptionReporter;

use App;
use Exception;

class ExceptionReporter
{
    public function report(Exception $exception): void
    {
        if ($this->shouldReport($exception)) {
            (new Receiver)->notify(new ExceptionThrown(
                get_class($exception),
                $exception->getFile(),
                $exception->getLine(),
                $exception->getMessage()
            ));
        }
    }

    private function shouldReport(Exception $exception): bool
    {
        return App::environment('production') &&
            !is_null(config('exception_reporter.slack_webhook_url'));
    }
}
