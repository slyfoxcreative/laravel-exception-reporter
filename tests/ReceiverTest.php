<?php

declare(strict_types=1);

namespace SlyFoxCreative\ReCaptcha\Tests;

use Orchestra\Testbench\TestCase;
use SlyFoxCreative\ExceptionReporter\Receiver;

class ReceiverTest extends TestCase
{
    public function testRouteNotificationForSlack()
    {
        config(['exception_reporter.slack_webhook_url' => 'https://example.com']);

        $receiver = new Receiver;
        $this->assertSame(
            'https://example.com',
            $receiver->routeNotificationForSlack()
        );
    }
}
